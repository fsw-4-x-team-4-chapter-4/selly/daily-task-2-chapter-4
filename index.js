const express = require('express')
const app = express()
// const fullStatik = require('serve-static')
const fs = require('fs')
// const path = require('path')
const port = 3000
const all_data = require('./dataSource')
const data1 = require ('./data1')
const data2 = require ('./data2')
const data3 = require ('./data3')
const data4 = require ('./data4')
const data5 = require('./data5')


app.set('view engine', 'ejs')
app.use(express.static('publik'))

// app.set('views', path.join(__dirname, '/publik'))



app.get('/', (req, res) => {
  res.render('home')
})

app.get('/about', (req, res) =>{
  res.render('about')
})

app.get('404', (req, res) =>{
  res.render('404')
})

app.get('/data', (req, res) => {
  res.render('table', {title : 'TABEL DATA', data : all_data})
})

app.get('/data1', (req, res) => {
  res.render('table', {title : 'TABEL DATA 1', data : data1})
})

app.get('/data2', (req, res) => {
  res.render('table', {title : 'TABEL DATA 2', data : data2})
})

app.get('/data3', (req, res) => {
  res.render('table', {title : 'TABEL DATA 3', data : data3})
})

app.get('/data4', (req, res) => {
  res.render('table', {title : 'TABEL DATA 4', data : data4})
})

app.get('/data5', (req, res) => {
  res.render('table', {title : 'TABEL DATA 5', data : data5})
})



app.use('/',(req, res) => {
  res.render('404')
  res.status(404)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})