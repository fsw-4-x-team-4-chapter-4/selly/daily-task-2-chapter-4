// filtering 1 dataSource
function datafilter1(data) {
    const result =[];
// data1 sebagai data filtering dari datasource
    for (let i=0; i < data.length; i++){
        if (data[i].eyeColor === "blue" && data[i].age >= 35 && data[i].age <= 40 && data[i].favoriteFruit === "apple"){
        
        result.push(data[i]);
        }
    }
    return result;
}

const data = require ("./dataSource.js");

module.exports = datafilter1(data);